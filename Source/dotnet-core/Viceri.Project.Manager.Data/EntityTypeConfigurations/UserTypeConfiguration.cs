﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class UserTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");

            builder.HasKey(i => i.Id).HasName("pk_User");

            builder.Property(i => i.Username)
                .HasColumnType("varchar(100)")
                .IsRequired();

            builder.Property(i => i.Email)
                .HasColumnType("varchar(100)")
                .IsRequired();

            builder.Property(i => i.Password)
                .HasColumnType("varchar(100)")
                .IsRequired();
        }
    }
}
