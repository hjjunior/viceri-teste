﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class ProjectTypeConfiguration : IEntityTypeConfiguration<Domain.Entities.Project>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.Project> builder)
        {
            builder.ToTable("Project");

            builder.HasKey(i => i.Id).HasName("pk_Project");

            builder.Property(i => i.Name)
               .HasColumnType("varchar(100)")
               .IsRequired();

            builder.Property(i => i.Description)
                .HasColumnType("varchar(1000)")
                .IsRequired();

            builder.Property(i => i.WebUrl)
               .HasColumnType("varchar(1000)")
               .IsRequired();

            builder.Property(i => i.Import_status)
                .HasColumnType("varchar(50)")
                .IsRequired();
        }
    }
}
