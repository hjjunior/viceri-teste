﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Viceri.Project.Manager.Data.Migrations
{
    public partial class Project : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Project",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GitLabId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(type: "varchar(1000)", nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", nullable: false),
                    WebUrl = table.Column<string>(type: "varchar(1000)", nullable: false),
                    Created_at = table.Column<DateTime>(nullable: false),
                    Last_activity_at = table.Column<DateTime>(nullable: false),
                    Forks_count = table.Column<int>(nullable: false),
                    Import_status = table.Column<string>(type: "varchar(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_Project", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Project");
        }
    }
}
