﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Data
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public ProjectRepository(ViceriProjectManagerContext context)
        {
            _context = context;
        }

        public async Task<List<int>> GetGitLabIdsImports()
        {
            return await _context.Projects.Select(x => x.GitLabId).ToListAsync();
        }

        public async Task<List<Domain.Entities.Project>> Add(List<Domain.Entities.Project> projects)
        {
            await _context.AddRangeAsync(projects);

            await _context.SaveChangesAsync();

            return await _context.Projects.OrderBy(x => x.Name).ToListAsync();
        }        
    }
}
