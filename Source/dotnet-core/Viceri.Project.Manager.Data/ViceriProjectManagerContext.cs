﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Viceri.Project.Manager.Data.EntityTypeConfigurations;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data
{
    public class ViceriProjectManagerContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Domain.Entities.Project> Projects { get; set; }

        public ViceriProjectManagerContext(DbContextOptions<ViceriProjectManagerContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTypeConfiguration());
        }
    }

    public class PrecoContextDesignFactory : IDesignTimeDbContextFactory<ViceriProjectManagerContext>
    {
        public ViceriProjectManagerContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ViceriProjectManagerContext>()
                .UseSqlServer("Server=localhost;Initial Catalog=GitLab;Persist Security Info=False;User ID=sa;Password=Pass@word;Connection Timeout=30;");

            return new ViceriProjectManagerContext(optionsBuilder.Options);
        }
    }
}
