﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IProjectService
    {
        Task<List<int>> GetGitLabIdsImports();
        Task<List<Domain.Entities.Project>> Add(List<Domain.Entities.Project> projects);
    }
}
