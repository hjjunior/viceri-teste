﻿using System.Collections.Generic;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IGitLabService
    {
        List<ProjectGitLab> GetProjects();
    }
}
