﻿using System;
using System.Collections.Generic;
using System.Text;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IGitLabRepository
    {
        List<ProjectGitLab> GetProjects();
    }
}
