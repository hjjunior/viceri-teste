﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IProjectRepository
    {
        Task<List<int>> GetGitLabIdsImports();
        Task<List<Domain.Entities.Project>> Add(List<Domain.Entities.Project> projects);
    }
}
