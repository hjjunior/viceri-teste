﻿using System;

namespace Viceri.Project.Manager.Domain.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public int GitLabId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string WebUrl { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Last_activity_at { get; set; }
        public int Forks_count { get; set; }
        public string Import_status { get; set; }
    }
}
