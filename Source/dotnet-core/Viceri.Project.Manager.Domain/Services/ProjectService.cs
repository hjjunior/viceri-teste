﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Domain.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public async Task<List<int>> GetGitLabIdsImports()
        {
            var models = await _projectRepository.GetGitLabIdsImports();
            return models;
        }

        public async Task<List<Domain.Entities.Project>> Add(List<Domain.Entities.Project> projects)
        {
            var models = await _projectRepository.Add(projects);
            return models;
        }
    }
}
