﻿using System.Collections.Generic;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Domain.Services
{
    public class GitLabService : IGitLabService
    {
        private readonly IGitLabRepository _gitLabRepository;

        public GitLabService(IGitLabRepository gitLabRepository)
        {
            _gitLabRepository = gitLabRepository;
        }

        public List<ProjectGitLab> GetProjects()
        {
            var models = _gitLabRepository.GetProjects();

            return models;
        }
    }
}
