﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> Login(string username, string password)
        {
            return await _userRepository.Login(username, password);
        }
    }
}
