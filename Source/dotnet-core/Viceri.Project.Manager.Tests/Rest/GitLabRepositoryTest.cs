﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;
using Xunit;

namespace Viceri.Project.Manager.Tests.Rest
{
    public class GitLabRepositoryTest
    {
        private readonly Mock<IGitLabService> _gitLabServiceMock;

        public GitLabRepositoryTest()
        {
            _gitLabServiceMock = new Mock<IGitLabService>();
        }

        [Fact]
        public void Get_projects_executed_with_success()
        {
            _gitLabServiceMock.Setup(x => x.GetProjects()).Returns(new List<ProjectGitLab>
            {
               new ProjectGitLab
               {
                    Id = 82500,
                    Name = "Projeto 1",
                    Description = "Descricao projeto 1",
                    Created_at = DateTime.Now,
                    Forks_count = 0,
                    Import_status = "none",
                    Last_activity_at = DateTime.Now,
                    WebUrl = ""                   
               },
               new ProjectGitLab
               {
                    Id = 82501,
                    Name = "Projeto 2",
                    Description = "Descricao projeto 1",
                    Created_at = DateTime.Now,
                    Forks_count = 0,
                    Import_status = "none",
                    Last_activity_at = DateTime.Now,
                    WebUrl = ""
               }
            });

            var gitLabApplication = new GitLabApplication(_gitLabServiceMock.Object);
            var projects =  gitLabApplication.ObterProjetos();

            Assert.NotNull(projects);
            Assert.Equal("Projeto 1", projects.FirstOrDefault().Name);
            Assert.Equal(2, projects.Count);
        }
    }
}
