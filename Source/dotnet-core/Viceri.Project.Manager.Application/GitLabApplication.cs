﻿using System;
using System.Collections.Generic;
using System.Text;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Application
{
    public class GitLabApplication : IGitLabApplication
    {
        private readonly IGitLabService _gitLabService;

        public GitLabApplication(IGitLabService gitLabService)
        {
            _gitLabService = gitLabService;
        }

        public List<ProjectGitLab> ObterProjetos()
        {
            var models = _gitLabService.GetProjects();

            return models;
        }
    }
}
