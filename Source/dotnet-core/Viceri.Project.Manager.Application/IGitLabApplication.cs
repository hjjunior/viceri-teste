﻿using System.Collections.Generic;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Application
{
    public interface IGitLabApplication
    {
        List<ProjectGitLab> ObterProjetos();
    }
}
