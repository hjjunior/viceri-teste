﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.Extensions;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Application
{
    public class ProjectApplication : IProjectApplication
    {
        private readonly IGitLabService _gitLabService;
        private readonly IProjectService _projectService;

        public ProjectApplication(IGitLabService gitLabService, IProjectService projectService)
        {
            _gitLabService = gitLabService;
            _projectService = projectService;
        }

        public async Task<List<Domain.Entities.Project>> IntegrateProjects()
        {
            //busca projetos do gitlab
            var projectsGitLab = _gitLabService.GetProjects();

            //busca os ids ja importados
            var idsProjectsGitLab = await _projectService.GetGitLabIdsImports();

            //filtra somente projetos com fork = 0 para contemplar a regra: Não permitir importar projetos criados via fork
            //filtra somente os projetos que nao foram importados para contemplar a regra: Não permitir importar projetos duplicados
            var projects = projectsGitLab.Where(x => x.Forks_count == 0 && !idsProjectsGitLab.Contains(x.Id)).EnumerableTo<Domain.Entities.Project>().ToList();
            
            var models = await _projectService.Add(projects);
            return models;
        }
    }
}
