﻿using System;
using System.Collections.Generic;
using System.Text;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Application.Mappings
{
    public class MappingProfile : AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<Domain.Entities.User, ViewModels.UserViewModel>();
            CreateMap<ProjectGitLab, Domain.Entities.Project>().ForMember(dest => dest.GitLabId, opt => opt.MapFrom(src => src.Id)).ForMember(dest => dest.Id, opt => opt.Ignore());
        }
    }
}
