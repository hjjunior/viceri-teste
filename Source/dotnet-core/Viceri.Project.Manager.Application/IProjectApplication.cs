﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Application
{
    public interface IProjectApplication
    {
        Task<List<Domain.Entities.Project>> IntegrateProjects();
    }
}