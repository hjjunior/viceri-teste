﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application;

namespace Viceri.Project.Manager.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Project/")]
    public class ProjectController : Controller
    {
        private readonly IProjectApplication _projectApplication;

        public ProjectController(IProjectApplication projectApplication)
        {
            _projectApplication = projectApplication;
        }

        [HttpGet]
        [Route("integration")]
        [ProducesResponseType(typeof(List<Domain.Entities.Project>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var models = await _projectApplication.IntegrateProjects();
            return Ok(models);
        }
    }
}