﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Rest.Repository
{
    public class GitLabRepository : RepositoryBase, IGitLabRepository
    {
        private readonly IConfiguration _configuration;
        public GitLabRepository(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;
            BaseUrl = new Uri(configuration["GitLabUrlBase"]);
        }

        public List<ProjectGitLab> GetProjects()
        {
            var url = $"users/{ _configuration["GitLabUser"]}/projects";
            var projects = Get<ProjectGitLab>(url);

            return projects;
        }
    }
}
