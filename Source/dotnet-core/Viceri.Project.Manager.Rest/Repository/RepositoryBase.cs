﻿using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Viceri.Project.Manager.Rest.Model;

namespace Viceri.Project.Manager.Rest.Repository
{
    public class RepositoryBase : RestClient
    {
        private readonly IConfiguration _configuration;
        public RepositoryBase(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private IRestResponse ExecuteOrThrowException(IRestRequest request)
        {
            AddAuthorizationToHeader(request);

            var response = Execute(request);

            if (response.StatusCode == HttpStatusCode.BadRequest)
                throw new Exception(response.Content);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new ApplicationException("Error retrieving response. Check inner details for more info.", response.ErrorException);

            return response;
        }

        private IRestResponse<T> ExecuteOrThrowException<T>(IRestRequest request)
            where T : new()
        {
            AddAuthorizationToHeader(request);

            var response = Execute<T>(request);

            if (response.StatusCode == HttpStatusCode.BadRequest)
                throw new Exception(response.Content);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new ApplicationException("Error retrieving response. Check inner details for more info.", response.ErrorException ?? new Exception(response.Content));

            return response;
        }

        public IRestResponse<T> Post<T>(string resource, object model)
            where T : new()
        {
            var requisicao = new RestRequest(resource, Method.POST);
            requisicao.AddJsonBody(model);

            return ExecuteOrThrowException<T>(requisicao);
        }
        public void Put<T>(string resource, int id, T model)
            where T : new()
        {
            var requisicao = new RestRequest(resource, Method.PUT);
            requisicao.AddQueryParameter("id", id.ToString());
            requisicao.AddJsonBody(model);

            ExecuteOrThrowException<T>(requisicao);
        }

        public T Put<T>(string resource, object model, params QueryStringParameter[] parameters)
            where T : new()
        {
            var requisicao = new RestRequest(resource, Method.PUT);
            requisicao.AddJsonBody(model);

            foreach (var parameter in parameters)
                requisicao.AddQueryParameter(parameter.Name, parameter.Value);

            var resposta = ExecuteOrThrowException<T>(requisicao);
            return resposta.Data;
        }

        public string Get(string resource, params QueryStringParameter[] parameters)
        {
            var requisicao = new RestRequest(resource, Method.GET);

            foreach (var parameter in parameters)
                requisicao.AddQueryParameter(parameter.Name, parameter.Value);

            var resposta = ExecuteOrThrowException(requisicao);

            if (resposta.Content == "null")
                return null;

            return resposta.Content;
        }

        public List<T> Get<T>(string resource, params QueryStringParameter[] parameters)
            where T : new()
        {
            var requisicao = new RestRequest(resource, Method.GET);

            foreach (var parameter in parameters)
                requisicao.AddQueryParameter(parameter.Name, parameter.Value);

            var resposta = ExecuteOrThrowException<List<T>>(requisicao);
            return resposta.Data;
        }

        public T GetFirstOrDefault<T>(string resource, params QueryStringParameter[] parameters)
            where T : class, new()
        {
            var result = Get<T>(resource, parameters);
            return result == null ? null : result.FirstOrDefault();
        }

        private void AddAuthorizationToHeader(IRestRequest requisicao)
        {
            requisicao.AddHeader("PRIVATE-TOKEN", (_configuration["GitLabPrivateToken"]));
        }
    }
}
