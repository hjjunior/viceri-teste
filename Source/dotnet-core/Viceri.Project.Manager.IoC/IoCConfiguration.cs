﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Data;
using Microsoft.Extensions.Configuration;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Domain.Services;
using Viceri.Project.Manager.Rest.Repository;

namespace Viceri.Project.Manager.IoC
{
    public static class IoCConfiguration
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            ConfigureDomain(services);
            ConfigureData(services, configuration);
            ConfigureApplication(services);
        }

        private static void ConfigureApplication(IServiceCollection services)
        {
            services.AddScoped<IUserApplication, UserApplication>();
            services.AddScoped<IGitLabApplication, GitLabApplication>();
            services.AddScoped<IProjectApplication, ProjectApplication>();
        }

        private static void ConfigureData(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ViceriProjectManagerContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IGitLabRepository, GitLabRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
        }

        private static void ConfigureDomain(IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IGitLabService, GitLabService>();
            services.AddScoped<IProjectService, ProjectService>();
        }
    }
}
